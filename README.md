## Hi 👋, I'm 3kh0

### "It's not a bug, it's a feature" - Andrew Tanenbaum

Welcome to my ~~GitHub~~ GitLab profile! Here I host some of my projects, you can see everything over at my website!

> https://3kh0.net

I use this profile to mainly host stuff that would get banned on GitHub 
